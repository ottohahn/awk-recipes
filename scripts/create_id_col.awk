#!/bin/awk -f
#
# program to add a new column with the same value to a file
BEGIN {FS=","; OFS=","; new_val = 9999;}
NR==1 { print $0,"new value"} # NR==1 indicates the first line
NR > 1 {print $0, new_val} # NR>1 indicates we ignore the first line, and print all
END{}     # End section

