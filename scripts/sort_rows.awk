#!/usr/bin/gawk -f
# GPLv3 appears here
# taken from: https://opensource.com/article/19/11/how-sort-awk
# usage: ./sorter.awk -v var=NUM FILE

BEGIN { FS=","; }
NR==1 {print $0}
NR>1 { # dump each field into an array
    ARRAY[$var] = $R;
}
END {
    asorti(ARRAY,SARRAY);
    # get length
    j = length(SARRAY);
    
    for (i = 2; i <= j; i++) {
        printf("%s\n",ARRAY[SARRAY[i]])
    }
}
