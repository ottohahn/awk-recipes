#!/bin/gawk
# PROGRAM: crud-create-ins-aft.awk
# AUTHOR: Otto Hahn Herrera
# DATE: 2020-06-12
# PURPOSE: Insert row after a given record (with phone 502-22-5977)

BEGIN { FS=":"; OFS=":"}

/502-22-5977/ { print $0;
                print "Bob:Howard:M:41:429-19-5982:1979-04-26:2023-06-12";
                next }
{ print $0 }
