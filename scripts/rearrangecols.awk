#!/bin/awk -f
#
# program to select and rearrange columns from a CSV file
# rearranges to give the full name and title as a space separated string
BEGIN {FS=","
    OFS=" "}  # Begin section: defines the separators for input/output
{print $4, $3, $2 }  # Loop section: selects and rearranges columns
END{}     # End section