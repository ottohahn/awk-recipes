BEGIN {FS=","; OFS=",";}
NR==1 { print $0,"total"} # NR==1 indicates the first line
NR > 1 { print $0, $2+$3+$4+$5+$6+$7} # column total is the sum of the rows
END{}     # End section
