#!/bin/gawk
# PROGRAM: append_row.awk
# AUTHOR: Otto Hahn Herrera
# DATE: 2020-06-12
# PURPOSE: Insert row at the end of a file with awk

BEGIN { FS=":"; OFS=":"}
{print $0}
END { print "Bob:Howard:M:41:429-19-5982:1979-04-26" }
