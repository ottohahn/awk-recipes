BEGIN {FS=","; OFS=",";}
NR==1 { print $0,"grant"} # NR==1 indicates the first line
NR > 1 { if ($2 <= 50 && $3 = "M") # NR>1 indicates we ignore the first line
    { print $0, 1 } # if statement checks for M and age <= 50
  else
    { print $0, 0 }} 
END{}     # End section

