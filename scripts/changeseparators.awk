#!/bin/awk -f
#
# program to change de file separator of a CSV file
# changes the file separator from comma to pipe
BEGIN {FS=","; OFS="|"}  # Begin section: defines the separators for input/output
    {print $1,$2,$3,$4} # prints all columns
END{}     # End section
