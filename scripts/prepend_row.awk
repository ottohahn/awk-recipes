#!/bin/gawk
# PROGRAM: crud-create-top.awk
# AUTHOR: Otto Hahn Herrera
# DATE: 2020-06-12
# PURPOSE: Insert record at second row with awk

BEGIN { FS=":"; OFS=":"}
NR == 2 { print "Bob:Howard:M:41:429-19-5982:1979-04-26:2023-06-12" }
{print $0}
