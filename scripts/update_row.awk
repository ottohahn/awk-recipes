#!/bin/gawk
# PROGRAMA: crud-update-one.awk
# AUTOR: Otto Hahn Herrera
# FECHA: 2020-06-13
# PROPOSITO: Actualizar registros en awk basados en una condicion

BEGIN { 
    FS=":"; 
    OFS=":";
    date="2023-06-2023"; 
}
$5=="382-26-2505"{ $2="Patterson";$7=date }
{ print $0 }
