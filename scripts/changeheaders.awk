#!/bin/awk -f
#
# program to change the file header of a CSV file
BEGIN {FS=","; OFS=",";
print "patient_id","Sex","Blood_Type","Age","Year_Of_Birth","Immunity"}
NR > 1 {print $0}  # NR>1 indicates we ignore the first line, and print all
END{}     # End section
