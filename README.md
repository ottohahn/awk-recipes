# awk recipes

## Description
A list of AWK recipes for doing all kinds of things with column based data.
It follows some standard recipes for reading, writing, editing and doing calculations. 


## Installation

All example files can be downloaded along with the file or files needed for execution, and then modified accordingly. 

| example | awk file | data file |
|---------|----------|-----------|
|Prints Hello World | helloworld.awk| None |
|Selecting and rearranging columns | rearrangecols.awk | rearrangecols.csv|
|Change the separator of a CSV file | changeseparators.awk | changeseparators.csv|
|Change the headers of a CSV file | changeheaders.awk | changeheaders.csv|
|Create new column with the same value| create_id_col.awk| create_id_col.csv|
|Create new column using a row condition| condition_col.awk| condition_col.csv|
|Create new column using a row calculation| calculate_col.awk| calculate_col.csv|
|Filter rows by value | filter_rows.awk| filter_rows.csv|
|Sort rows based on a value| sort_rows.awk| sort_rows.csv|
|Prepend a row|prepend_row.awk|prepend_row.csv|
|Append a row|append_row.awk|append_row.csv|
|Insert a row|insert_row.awk|insert_row.csv|
|Update a row|update_row.awk|update_row.csv|


## Usage

How to run the different examples:

| awk file | command |
|----------|---------|
| helloworld.awk | `awk -f helloworld.awk` |
| rearrangecols.awk | `awk -f rearrangecols.awk ../data/rearrangecols.csv` |
| changeseparators.awk | `awk -f changeseparators.awk ../data/changeseparators.csv` |
| changeheaders.awk | `awk -f changeheaders.awk ../data/changeheaders.csv` |
| create_id_col.awk | `awk -f create_id_col.awk ../data/create_id_col.csv` |
| condition_col.awk | `awk -f condition_col.awk ../data/condition_col.csv` |
| calculate_col.awk | `awk -f calculate_col.awk ../data/calculate_col.csv` |
| filter_rows.awk | `awk -f filter_rows.awk ../data/filter_rows.csv `|
| sort_rows.awk | `awk -f sort_rows.awk -v var=4 ../data/sort_rows.csv`|
|prepend_row.awk|`awk -f prepend_row.awk ../data/prepend_row.csv `|
|append_row.awk|`awk -f append_row.awk ../data/append_row.csv `|
|insert_row.awk|`awk -f insert_row.awk ../data/insert_row.csv `|
|update_row.awk|`awk -f update_row.awk ../data/update_row.csv `|
